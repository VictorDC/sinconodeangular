var userModel = libRequire('users/models');

module.exports.loadData = function() {
    var users = [{
        name: "Admin",
        email: "admin@sinco.com",
        password: "sinco1234",
        role: "admin"
    }, {
        name: "Superadmin",
        email: "superadmin@sinco.com",
        password: "sinco1234",
        role: "superadmin"
    }, {
        name: "User",
        email: "user@sinco.com",
        password: "sinco1234",
        role: "user"
    }];

    users.forEach(element => {
        var newUser = new userModel(element);

        userModel.addUser(newUser, (err, user) => {
            if (err) {
                console.log("Failed to register user");
            }
        });
    });
}