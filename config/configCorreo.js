const nodemailer = require('nodemailer');


module.exports = (req) => {
  var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
  user: 'acropolisst.up@gmail.com', // Cambialo por tu email
  pass: 'acropolisstartup' // Cambialo por tu password
  }
  });

 const mailOptions = {
  from: `"${req.nombre} ${req.apellido} 👻" <${req.correoElectronico}>`,
  to: 'miguelmedina03@gmail.com', // Cambia esta parte por el destinatario
  subject: 'Contacto Pagina SINCO',
  html: `
  <strong>Nombre:</strong> ${req.nombre} <br/>
  <strong>Apellido:</strong> ${req.apellido} <br/>
  <strong>E-mail:</strong> ${req.correoElectronico} <br/>
  <strong>Organización:</strong> ${req.organizacion} <br/>
  <strong>Mensaje:</strong> ${req.mensajeContacto}
  `
  };


 transporter.sendMail(mailOptions, function (err, info) {
  if (err)
  console.log(err)
  else
  console.log(info);
  });
 }
