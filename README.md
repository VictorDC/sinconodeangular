# Sinco

### Versiones
- npm: 6.4.1
- node: 8.14.0
- MongoDB: 4.0.4

### Instalar e inicializar
```
npm install
cp .env.example .env
```

Completar información del .env

```
npm run db:seeds
```

### Correr
```
npm run start
```