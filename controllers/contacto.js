const Contacto = require('../models/contacto');

exports.postContacto = (req, res, next) => {
  const nombre = req.body.nombre;
  const apellido = req.body.apellido;
  const correo = req.body.correoElectronico;
  const org = req.body.organizacion;
  const msg = req.body.mensajeContacto;
  const contacto = new Contacto({
    nombre: nombre,
    apellido: apellido,
    correoElectronico: correo,
    organizacion: org,
    mensajeContacto: msg
  });
  contacto.save().then(result => {
    res.status(201).json(result);
  }).catch(err => console.log(err));
}
