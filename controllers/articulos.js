const Articulo = require('../models/articulo');

/**
*@api {get} /articulos Obtener lista de articulos
*@apiName GetArticulos
*@apiGroup Articulos
*
*@apiSuccess {Object[]} articulos Lista de articulos.
 */
exports.getArticulos = (req, res, next) => {
    Articulo.find().then(articulos => {
        res.status(200).json({articulos: articulos});
    }).catch(err => console.log(err));
}

/**
*@api {get} /articulo Obtener articulo por id
*@apiName GetArticulo
*@apiGroup Articulo
*
*@apiParam {Number} id ID del articulo.
*@apiSuccess {Object} articulo Informacion del articulo.
*@apiSuccess {String} articulo.completa Articulo completo.
*@apiSuccess {Object} articulo.vistaPrevia Informacion para vista previa del articulo.
*@apiSuccess {String} articulo.vistaPrevia.titulo Titulo del articulo.
*@apiSuccess {String} articulo.vistaPrevia.contenido Resumen para vista previa del articulo.
*@apiSuccess {String} articulo.vistaPrevia.imgUrl Url de la imagen miniatura del articulo.
 */
exports.getArticulo = (req, res, next) => {
    const id = req.params.id;
    Articulo.findById(id).then(articulo => {
        res.status(200).json({articulo: articulo});
    }).catch(err => console.log(err));
}

/**
*@api {post} /articulo Crear articulo
*@apiName PostArticulo
*@apiGroup Articulo
 */
exports.postArticulo = (req, res, next) => {
    const titulo = req.body.vistaPrevia.titulo;
    const contenido = req.body.vistaPrevia.contenido;
    const imgUrl = req.body.vistaPrevia.imgUrl;
    console.log(imgUrl);
    const completa =  req.body.completa
    const articulo = new Articulo({
        completa: completa,
        vistaPrevia: {
            titulo: titulo,
            contenido: contenido,
            imgUrl: imgUrl
        }
    });
    articulo.save().then(result => {
        res.status(201).json(result);
    }).catch(err => console.log(err));
}

/**
*@api {put} /articulo Editar articulo
*@apiName PutArticulo
*@apiGroup Articulo
 */
exports.editArticulo = (req, res, next) => {
    const id = req.params.id;
    const titulo = req.body.vistaPrevia.titulo;
    const contenido = req.body.vistaPrevia.contenido;
    const imgUrl = req.body.vistaPrevia.imgUrl;
    console.log(imgUrl);
    const completa =  req.body.completa
    Articulo.findById(id).then(articulo => {
        articulo.completa = completa;
        articulo.vistaPrevia = {
            titulo: titulo,
            contenido: contenido,
            imgUrl: imgUrl
        }
        return articulo.save();
    }).then(result => {
        res.status(200).json(result);
    }).catch(err => console.log(err));
}

/**
*@api {delete} /articulo Eliminar articulo
*@apiName DeleteArticulo
*@apiGroup Articulo
 */
exports.deleteArticulo = (req, res, next) => {
    const id = req.params.id;
    Articulo.findByIdAndRemove(id).then(result => {
        res.status(200).json(result);
    });
}

/**
*@api {post} /uploadImg Subir imagen
*@apiName PostUploadImg
*@apiGroup Image
 */
exports.uploadImage = (req, res, next) => {
    console.log('Imagen cargada',req.file);
    const imgPath = req.file.path;
    res.json(imgPath)
}
