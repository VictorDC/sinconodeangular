const express = require('express');

const contactoController = require('../controllers/contacto');

const router = express.Router();

router.post('/contacto', contactoController.postContacto);

module.exports = router;
