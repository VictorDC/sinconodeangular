const express = require('express');

const correoController = require('../controllers/configCorreo');

const router = express.Router();

router.post('/enviarcorreo', correoController.sendMesaje);

module.exports = router;
