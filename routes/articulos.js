const express = require('express');

const articulosController = require('../controllers/articulos');

const router = express.Router();

router.get('/articulos', articulosController.getArticulos);

router.get('/articulo/:id', articulosController.getArticulo);
router.post('/articulo', articulosController.postArticulo);
router.put('/articulo/:id', articulosController.editArticulo);
router.delete('/articulo/:id', articulosController.deleteArticulo);

router.post('/uploadImg',articulosController.uploadImage);

module.exports = router;