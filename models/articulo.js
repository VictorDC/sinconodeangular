const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articuloSchema = new Schema({
    completa: String,
    vistaPrevia: {
        titulo: {
            type: String,
            required: true
        },
        contenido: {
            type: String,
            required: true
        },
        imgUrl: String
    }
},
{
    timestamps: true
});

module.exports = mongoose.model('Articulo', articuloSchema)