const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contactoSchema = new Schema({
  nombre: String,
  apellido: String,
  correoElectronico: String,
  organizacion: String,
  mensajeContacto: String, //Preguntar si quieren guardarlo
},
{
  timestamps: true
});

module.exports = mongoose.model('Contacto', contactoSchema)
